import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public profileForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.initForm();
    this.loadForm();
  }

  private initForm() {
    this.profileForm = this.formBuilder.group({
      descriptionOne: [''],
      descriptionTwo: [''],
      descriptionThree: [''],
      descriptionFour: ['']
    });
  }

  private loadForm() {
    this.profileForm.patchValue({
      descriptionOne: this.loadDescriptionOne(),
      descriptionTwo: this.loadDescriptionTwo(),
      descriptionThree: this.loadDescriptionThree(),
      descriptionFour: this.loadDescriptionFour()
    })
  }

  private loadDescriptionOne(){
    return `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus semper eu tortor nec hendrerit. Pellentesque ut mauris eu nisl malesuada malesuada. Etiam eu orci eget dolor ornare eleifend eget in mi. Donec a elit ac purus mattis interdum. Duis euismod nulla at porta dapibus. Morbi at nibh consequat, dictum nunc vel, dictum quam. Ut imperdiet eros ut nulla imperdiet feugiat. Mauris hendrerit ex eget urna ullamcorper, a facilisis massa bibendum.`;
  }
  
  private loadDescriptionTwo(){
    return `Vestibulum quis elit urna. Donec quis tincidunt elit. Praesent porta interdum dui at porta. Nulla eget ullamcorper erat, nec imperdiet mauris. Mauris eu turpis porta, maximus eros id, mattis ipsum. Mauris viverra, augue a placerat vulputate, nibh eros rhoncus enim, nec sodales nibh sem a orci. Maecenas ac erat non arcu ultrices porttitor. Vestibulum ut dui libero. Phasellus massa ipsum, vestibulum et consectetur non, malesuada non risus. Pellentesque porta massa quis dui dignissim, sed scelerisque nisl convallis. Curabitur efficitur auctor euismod. Donec dignissim eget leo in tempus. Aliquam feugiat mi quis velit efficitur, varius tristique quam porta`;
  }

  private loadDescriptionThree(){
    return `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus semper eu tortor nec hendrerit. Pellentesque ut mauris eu nisl malesuada malesuada. Etiam eu orci eget dolor ornare eleifend eget in mi. Donec a elit ac purus mattis interdum. Duis euismod nulla at porta dapibus. Morbi at nibh consequat, dictum nunc vel, dictum quam. Ut imperdiet eros ut nulla imperdiet feugiat. Mauris hendrerit ex eget urna ullamcorper, a facilisis massa bibendum.

    Vestibulum quis elit urna. Donec quis tincidunt elit. Praesent porta interdum dui at porta. Nulla eget ullamcorper erat, nec imperdiet mauris. Mauris eu turpis porta, maximus eros id, mattis ipsum. Mauris viverra, augue a placerat vulputate, nibh eros rhoncus enim, nec sodales nibh sem a orci. Maecenas ac erat non arcu ultrices porttitor. Vestibulum ut dui libero. Phasellus massa ipsum, vestibulum et consectetur non, malesuada non risus. Pellentesque porta massa quis dui dignissim, sed scelerisque nisl convallis. Curabitur efficitur auctor euismod. Donec dignissim eget leo in tempus. Aliquam feugiat mi quis velit efficitur, varius tristique quam porta.`;
  }

  private loadDescriptionFour(){
    return `Nulla et justo id risus malesuada tempor. Nullam commodo iaculis felis, ut luctus dolor mattis ac. Duis rutrum vulputate lobortis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae`;
  }

}
